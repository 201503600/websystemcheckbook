package com.edcil.Checkbook.service;

import com.edcil.Checkbook.entity.AccountType;
import com.edcil.Checkbook.repository.AccountTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class AccountTypeService {
    @Autowired
    AccountTypeRepository accountTypeRepository;

    public List<AccountType> getAllAccountType(){
        return accountTypeRepository.findAll();
    }

    public Optional<AccountType> getAccountType(Long id){
        return accountTypeRepository.findById(id);
    }

    public void saveOrUpdate(AccountType accountType){
        accountTypeRepository.save(accountType);
    }

    public void delete(Long id){
        accountTypeRepository.deleteById(id);
    }
}
