package com.edcil.Checkbook.service;

import com.edcil.Checkbook.entity.Account;
import com.edcil.Checkbook.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class AccountService {
    @Autowired
    AccountRepository accountRepository;

    public List<Account> getAllAccount(){
        return accountRepository.findAll();
    }

    public Optional<Account> getAccount(Long id){
        return accountRepository.findById(id);
    }

    public void saveOrUpdate(Account account){
        accountRepository.save(account);
    }

    public void delete(Long id){
        accountRepository.deleteById(id);
    }
}
