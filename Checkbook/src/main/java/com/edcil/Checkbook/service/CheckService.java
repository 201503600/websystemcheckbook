package com.edcil.Checkbook.service;

import com.edcil.Checkbook.entity.Check;
import com.edcil.Checkbook.repository.CheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CheckService {
    @Autowired
    CheckRepository checkRepository;

    public List<Check> getAllCheck(){
        return checkRepository.findAll();
    }

    public Optional<Check> getCheck(Long id){
        return checkRepository.findById(id);
    }

    public void saveOrUpdate(Check account){
        checkRepository.save(account);
    }

    public void delete(Long id){
        checkRepository.deleteById(id);
    }
}
