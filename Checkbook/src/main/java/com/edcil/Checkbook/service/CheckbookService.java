package com.edcil.Checkbook.service;

import com.edcil.Checkbook.entity.Checkbook;
import com.edcil.Checkbook.repository.CheckbookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CheckbookService {
    @Autowired
    CheckbookRepository checkbookRepository;

    public List<Checkbook> getAllCheckbook(){
        return checkbookRepository.findAll();
    }

    public Optional<Checkbook> getCheckbook(Long id){
        return checkbookRepository.findById(id);
    }

    public void saveOrUpdate(Checkbook account){
        checkbookRepository.save(account);
    }

    public void delete(Long id){
        checkbookRepository.deleteById(id);
    }
}
