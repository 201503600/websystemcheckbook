package com.edcil.Checkbook;

import com.edcil.Checkbook.entity.AccountType;
import com.edcil.Checkbook.repository.AccountTypeRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class StartRunner implements ApplicationRunner {
    @Autowired
    private AccountTypeRepository accountTypeRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        AccountType monetaryAccount = new AccountType("Monetaria");
        AccountType savingAccount = new AccountType("Ahorro");

        accountTypeRepository.save(monetaryAccount);
        accountTypeRepository.save(savingAccount);
    }
}
