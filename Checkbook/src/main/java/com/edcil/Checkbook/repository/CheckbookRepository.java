package com.edcil.Checkbook.repository;

import com.edcil.Checkbook.entity.Checkbook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CheckbookRepository extends JpaRepository<Checkbook, Long> {
}
