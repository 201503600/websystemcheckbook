package com.edcil.Checkbook.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_checkbook")
public class Checkbook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCheckbook")
    private Long idCheckbook;

    @JoinColumn(name = "FK_ACCOUNT", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Account account;

    @Column(name = "checkbookNumber", nullable = false, length = 20)
    private Long checkbook;
}
