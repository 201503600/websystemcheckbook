package com.edcil.Checkbook.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_account_type")
public class AccountType {

    public AccountType() { }
    public AccountType(String type) {
        this.type = type;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idAccountType")
    private Long idAccountType;

    @Column(name = "accountType", unique = true, nullable = false)
    private String type;
}
