package com.edcil.Checkbook.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_check")
public class Check {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCheck")
    private Long idCheck;

    @JoinColumn(name = "FK_CHECKBOOK", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Checkbook checkbook;

    @Column(name = "checkNumber", nullable = false)
    private Long check;

    @Column(name = "amount", nullable = false, length = 16, precision = 2)
    private Double amount;
}
