package com.edcil.Checkbook.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "tbl_account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idAccount")
    private Long idAccount;

    @JoinColumn(name = "FK_CLIENT", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Client client;

    @JoinColumn(name = "FK_ACCOUNT_TYPE", nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private AccountType accountType;

    @Column(name = "amount", nullable = false, length = 16, precision = 2)
    private Double amount;
}
