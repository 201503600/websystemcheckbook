package com.edcil.Checkbook;

import com.edcil.Checkbook.entity.AccountType;
import com.edcil.Checkbook.repository.AccountTypeRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckbookApplication {
	
	public static void main(String[] args) {

		SpringApplication.run(CheckbookApplication.class, args);


	}

}
