package com.edcil.Checkbook.controller;

import com.edcil.Checkbook.entity.AccountType;
import com.edcil.Checkbook.service.AccountTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/account-type")
@CrossOrigin(origins = "*")
public class AccountTypeController {
    @Autowired
    private AccountTypeService accountTypeService;

    @GetMapping
    public List<AccountType> getAll(){
        return accountTypeService.getAllAccountType();
    }

    @GetMapping("/{clientId}")
    public Optional<AccountType> getClient(@PathVariable("clientId") Long accountTypeId){
        return accountTypeService.getAccountType(accountTypeId);
    }

    @PostMapping
    public void addClient(@RequestBody AccountType accountType){
        accountTypeService.saveOrUpdate(accountType);
    }

    @DeleteMapping("/{clientId}")
    public void deleteClient(@PathVariable("clientId") Long accountTypeId){
        accountTypeService.delete(accountTypeId);
    }
}
