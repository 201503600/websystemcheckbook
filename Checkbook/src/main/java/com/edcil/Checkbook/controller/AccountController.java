package com.edcil.Checkbook.controller;

import com.edcil.Checkbook.entity.Account;
import com.edcil.Checkbook.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/account")
@CrossOrigin(origins = "*")
public class AccountController {
    @Autowired
    private AccountService accountService = new AccountService();

    @GetMapping
    public List<Account> getAll(){
        return accountService.getAllAccount();
    }

    @GetMapping("/{accountId}")
    public Optional<Account> getAccount(@PathVariable("accountId") Long accountId){
        return accountService.getAccount(accountId);
    }

    @PostMapping
    public void addAccount(@RequestBody Account account){
        accountService.saveOrUpdate(account);
    }

    @DeleteMapping("/{accountId}")
    public void deleteAccount(@PathVariable("accountId") Long accountId){
        accountService.delete(accountId);
    }
}
