package com.edcil.Checkbook.controller;

import com.edcil.Checkbook.entity.Check;
import com.edcil.Checkbook.service.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/check")
@CrossOrigin(origins = "*")
public class CheckController {
    @Autowired
    private CheckService checkService = new CheckService();

    @GetMapping
    public List<Check> getAll(){
        return checkService.getAllCheck();
    }

    @GetMapping("/{clientId}")
    public Optional<Check> getClient(@PathVariable("clientId") Long checkId){
        return checkService.getCheck(checkId);
    }

    @PostMapping
    public void addClient(@RequestBody Check check){
        checkService.saveOrUpdate(check);
    }

    @DeleteMapping("/{clientId}")
    public void deleteClient(@PathVariable("clientId") Long checkId){
        checkService.delete(checkId);
    }
}
