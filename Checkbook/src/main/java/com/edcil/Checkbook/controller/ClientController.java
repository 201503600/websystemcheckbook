package com.edcil.Checkbook.controller;

import com.edcil.Checkbook.entity.Client;
import com.edcil.Checkbook.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/client")
@CrossOrigin(origins = "*")
public class ClientController {
    @Autowired
    private ClientService clientService =  new ClientService();

    @GetMapping
    public List<Client> getAll(){
        return clientService.getAllClient();
    }

    @GetMapping("/{clientId}")
    public Optional<Client> getClient(@PathVariable("clientId") Long clientId){
        return clientService.getClient(clientId);
    }

    @PostMapping
    public void addClient(@RequestBody Client client){
        clientService.saveOrUpdate(client);
    }

    @DeleteMapping("/{clientId}")
    public void deleteClient(@PathVariable("clientId") Long clientId){
        clientService.delete(clientId);
    }

}
