package com.edcil.Checkbook.controller;

import com.edcil.Checkbook.entity.Checkbook;
import com.edcil.Checkbook.service.CheckbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/checkbook")
@CrossOrigin(origins = "*")
public class CheckbookController {
    @Autowired
    private CheckbookService checkbookService = new CheckbookService();

    @GetMapping
    public List<Checkbook> getAll(){
        return checkbookService.getAllCheckbook();
    }

    @GetMapping("/{clientId}")
    public Optional<Checkbook> getClient(@PathVariable("clientId") Long checkbookId){
        return checkbookService.getCheckbook(checkbookId);
    }

    @PostMapping
    public void addClient(@RequestBody Checkbook checkbook){
        checkbookService.saveOrUpdate(checkbook);
    }

    @DeleteMapping("/{clientId}")
    public void deleteClient(@PathVariable("clientId") Long checkbookId){
        checkbookService.delete(checkbookId);
    }
}
