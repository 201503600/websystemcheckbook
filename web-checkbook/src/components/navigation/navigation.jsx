import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import React,{ useState } from 'react';
import Button from 'react-bootstrap/Button';
import { useNavigate } from "react-router-dom";

function Navigation() {
  let navigate = useNavigate();

  return (
    <Navbar bg="dark" variant="dark">
      <Container>
      <Navbar.Brand href="/">Checkbook Web</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Nav >
            <Nav.Link href="/client">Clientes</Nav.Link>
            <Nav.Link href="/account">Cuentas</Nav.Link>
            <Nav.Link href="/checkbook">Chequeras</Nav.Link>
            <Nav.Link href="/check">Cheques</Nav.Link>
          </Nav>          
        </Navbar.Collapse>   
      </Container>
    </Navbar>

  );
}

export default Navigation;