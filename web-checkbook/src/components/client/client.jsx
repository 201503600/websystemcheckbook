import Container from "react-bootstrap/esm/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Alert } from "react-bootstrap";
import { AiOutlineUser } from "react-icons/ai";

function Client() {
  const ApiUrl = "http://127.0.0.1:8080";
  const [validated, setValidated] = useState(false);
  let navigate = useNavigate();

  const [showAlert, setShowAlert] = useState(false);
  const [alertText, setAlertText] = useState("");
  const [alertClass, setAlertClass] = useState("");

  const [idClient, setIdClient] = useState(0);
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");

  const [arrayClient, setArrayClient] = useState([]);

  useEffect(() => {
    let timeout;
    if (showAlert) {
      timeout = setTimeout(() => {
        setShowAlert(false);
      }, 3000);
    }
    clearTimeout(timeout);

    updateArray();
  }, [showAlert]);

  const updateArray = () => {
    fetch(`${ApiUrl}/api/client`, { method: "GET" }).then((response) => {
      return response
        .json()
        .then((data) => {
          setArrayClient(data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  const ShowMsg = (alertClass, alertText) => {
    setShowAlert(true);
    setAlertClass(alertClass);
    setAlertText(alertText);
  };

  function soloLetras(event) {
    const pattern = /^[a-zA-Z]+$/;
    const inputChar = event.key;

    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  function soloNumeros(event) {
    const pattern = /^[0-9]+$/;
    const inputChar = event.key;

    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  function noCaracteres(event) {
    event.target.value = event.target.value.replace(/[^a-zA-Z]/g, "");
  }

  const handleSubmitRegister = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    event.stopPropagation();
    setValidated(true);

    if (form.checkValidity() === false) {
      ShowMsg("danger", "Llene todos los datos, no puede dejar campos vacios");
      return;
    }

    let data = {
      email: email,
      firstName: firstName,
      lastName: lastName,
      address: address,
      phone: phone,
    };

    let requestOptionsPOST = {
      method: "POST",
      body: JSON.stringify(data),
    };
    console.log(requestOptionsPOST);

    fetch(`${ApiUrl}/api/client`, requestOptionsPOST).then((response) => {
      return response
        .json()
        .then((data) => {
          console.log(response);
          console.log(data);
          if (response.status === 200) {
            updateArray();
          } else {
            ShowMsg("danger", "Error inesperado en el servidor");
          }

          return;
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  return (
    <Container>
      <h2>
        <AiOutlineUser />
        Clientes
      </h2>

      {showAlert && (
        <Alert
          className={`alert alert-${alertClass} alert-dismissible fade show`}
          onClose={() => setShowAlert(false)}
          dismissible
        >
          {alertText}
        </Alert>
      )}

      <Form noValidate validated={validated} onSubmit={handleSubmitRegister}>
        <Row xs={2} sm={2} md={2} lg={2} className="g-4">
          <Col>
            <Form.Group className="mb-3 registro-input">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresar nombre"
                tabIndex={2}
                onKeyDown={soloLetras}
                onInput={noCaracteres}
                required
                autoFocus
                onChange={(e) => setFirstName(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Ingrese un nombre
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group className="mb-3 registro-input">
              <Form.Label>Correo</Form.Label>
              <Form.Control
                type="email"
                placeholder="Ingresar correo"
                tabIndex={4}
                required
                onChange={(e) => setEmail(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Ingrese un correo válido
              </Form.Control.Feedback>
            </Form.Group>
          </Col>

          <Col>
            <Form.Group className="mb-3 registro-input">
              <Form.Label>Apellido</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresar apellido"
                tabIndex={3}
                onKeyDown={soloLetras}
                onInput={noCaracteres}
                required
                onChange={(e) => setLastName(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Ingrese un apellido
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group className="mb-3 registro-input">
              <Form.Label>Teléfono</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresar teléfono"
                tabIndex={5}
                onKeyDown={soloNumeros}
                /*onInput={noCaracteres}*/ required
                onChange={(e) => setPhone(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Ingrese un teléfono
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col>
            <Form.Group className="mb-3 registro-input">
              <Form.Label>Dirección</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresar dirección"
                tabIndex={6}
                required
                onChange={(e) => setAddress(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Ingrese una dirección
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button variant="primary" type="submit">
              Crear cliente
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default Client;
