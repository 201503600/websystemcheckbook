import Container from 'react-bootstrap/esm/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import {Alert} from 'react-bootstrap';
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { ImUserTie } from 'react-icons/im';
import { AiOutlineBook } from 'react-icons/ai';


function RegistroUsuario() {
    const ApiUrl = "http://127.0.0.1:8080";
  const [validated, setValidated] = useState(false);
  let navigate = useNavigate();

  const [showAlert, setShowAlert] = useState(false);
  const [alertText, setAlertText] = useState("");
  const [alertClass, setAlertClass] = useState("");

  const [idCheckbook, setIdCheckbook] = useState(0);
  const [idAccount, setIdAccount] = useState("");
  const [checkbook, setCheckbook] = useState("");

  const [arrayCheckbook, setArrayCheckbook] = useState([]);
  const [arrayAccount, setArrayAccount] = useState([]);

  useEffect(() => {
    let timeout;
    if (showAlert) {
      timeout = setTimeout(() => {
        setShowAlert(false);
      }, 3000);
    }
    clearTimeout(timeout);

    updateArray();
    fetch(`${ApiUrl}/api/account`, { method: "GET" }).then((response) => {
      return response
        .json()
        .then((data) => {
          setArrayAccount(data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  }, [showAlert]);

  const updateArray = () => {
    fetch(`${ApiUrl}/api/checkbook`, { method: "GET" }).then((response) => {
      return response
        .json()
        .then((data) => {
          setArrayCheckbook(data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  const ShowMsg = (alertClass, alertText) => {
    setShowAlert(true);
    setAlertClass(alertClass);
    setAlertText(alertText);
  };

  function soloLetras(event) {
    const pattern = /^[a-zA-Z]+$/;
    const inputChar = event.key;

    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  function soloNumeros(event) {
    const pattern = /^[0-9]+$/;
    const inputChar = event.key;

    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  function noCaracteres(event) {
    event.target.value = event.target.value.replace(/[^a-zA-Z]/g, "");
  }

  const handleSubmitRegister = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    event.stopPropagation();
    setValidated(true);

    if (form.checkValidity() === false) {
      ShowMsg("danger", "Llene todos los datos, no puede dejar campos vacios");
      return;
    }

    let data = {
      account: {},
      checkbook: checkbook,
    };

    let requestOptionsPOST = {
      method: "POST",
      body: JSON.stringify(data),
    };
    console.log(requestOptionsPOST);

    fetch(`${ApiUrl}/api/checkbook`, requestOptionsPOST).then((response) => {
      return response
        .json()
        .then((data) => {
          console.log(response);
          console.log(data);
          if (response.status === 200) {
            updateArray();
          } else {
            ShowMsg("danger", "Error inesperado en el servidor");
          }

          return;
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  return (
    <Container>
      <h2>
        <AiOutlineBook />
        Chequeras
      </h2>

      {showAlert && (
        <Alert
          className={`alert alert-${alertClass} alert-dismissible fade show`}
          onClose={() => setShowAlert(false)}
          dismissible
        >
          {alertText}
        </Alert>
      )}

      <Form noValidate validated={validated} onSubmit={handleSubmitRegister}>
        <Row xs={2} sm={2} md={2} lg={2} className="g-4">
          <Col>
            <Form.Group className="mb-3 registro-input">
              <Form.Label>Cuenta</Form.Label>
              <Form.Select
                type="text"
                tabIndex={2}
                required
                autoFocus
                onChange={(e) => setIdAccount(e.target.value)}
              >
                <option>Seleccione una opción</option>
                {
                    arrayAccount.map((account) => (
                        <option value={account.idAccount}>{account.firstName + " - " + account.client.firstName}</option>
                    ))
                }
              </Form.Select>
              <Form.Control.Feedback type="invalid">
                Seleccione una cuenta
              </Form.Control.Feedback>
            </Form.Group>
          </Col>

          <Col>
            <Form.Group className="mb-3 registro-input">
              <Form.Label>Número chequera</Form.Label>
              <Form.Control
                type="text"
                tabIndex={3}
                required
                onKeyDown={soloNumeros}
                onChange={(e) => setCheckbook(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Seleccione un tipo de cuenta
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button variant="primary" type="submit">
              Asignar chequera
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
}

export default RegistroUsuario;