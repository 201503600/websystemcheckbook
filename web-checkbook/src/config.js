const config = {
  url: "http://localhost",
  port_serve: ":8080",
};

export default {
  apiUrl: `${config.url}${config.port_server}`,
  requestOptionsPOST: {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: {},
  },
  requestOptionsGET: {
    method: "GET",
    headers: {},
  },
};
