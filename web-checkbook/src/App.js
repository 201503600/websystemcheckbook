import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route, Navigate } from "react-router-dom";
import Navigation from './components/navigation/navigation';
import Account from './components/account/account';
import RegistroRepartidor from './components/checkbook/checkbook';
import RegistroEmpresa from './components/check/check';
import Client from './components/client/client';

function App() {
  return (
    <div className="App">
      <Navigation/>
        <Routes>
          {/* Rutas para cada página */} 
          <Route path='/' element={< Client/>}  />
          <Route path='/client' element={< Client/>}  />
          <Route path='/account' element={< Account/>}  />
          <Route path='/checkbook' element={< RegistroRepartidor/>}  />
          <Route path='/check' element={< RegistroEmpresa/>}  />
          <Route path='*' element={<Navigate to='/' />} />
        </Routes>      
    </div>
  );
}

export default App;
